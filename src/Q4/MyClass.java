package Q4;

public class MyClass {

	public void methX() throws DataException{
		throw new DataException();
	}
	
	public void methY() throws FormatException{
		throw new FormatException();
	}
	
	
	public static void main(String[] args) {
		try{
		MyClass my = new MyClass();
		System.out.println("a");
		my.methX();
		System.out.println("b");
		my.methY();
		System.out.println("c");
		return;
		
		}catch(DataException e){
			System.out.println("d");

		}catch(FormatException e){
			System.out.println("e");

		}finally{
			System.out.println("f");

		}
		System.out.println("g");

		
	}
	// Question2:  print "a" then "b" then "c" then print "f" then "g"
	// Question3 print "a" then  "d" then "f" then "g"
	// Question4 print "a" then  "b"then  "e"then  "f" then "g"

}
