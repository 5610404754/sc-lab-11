package Q5;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Refrigerator r1 = new Refrigerator();

			r1.put("fish");

			r1.put("bird");

			r1.put("pig");

			r1.put("cake");
			System.out.println(r1.toString());
			System.out.println("Returning pig item: " + r1.takeOut("pig"));
			System.out.println("Returning cake item: " + r1.takeOut("cake"));
		
	}

}
