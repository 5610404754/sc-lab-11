package Q5;

import java.util.ArrayList;

public class Refrigerator {
	int size = 3;
	int putIn = 1;
	ArrayList<String> things = new ArrayList<String>();

	public void put(String stuff) {
		try {
			if (putIn <= 3) {
				things.add(stuff);
				putIn++;
			} else {
				throw new FullException("Error rfrigerator");

			}

		} catch (FullException e) {
			System.err.println("refrigerator is full");
		}

	}

	public String takeOut(String stuff) {
		if (!things.contains(stuff)) {
			return null;
		} else {
			return stuff;
		}

	}

	public String toString() {
		return things.toString();
	}

}
