package Q3;

import java.util.HashMap;


public class WordCounterMain {

	String message;
	HashMap<String, Integer> wordCount = new HashMap<String, Integer>();

	public WordCounterMain(String message) {
		this.message = message;
	}

	public static void main(String[] args) {
		WordCounterMain counter = new WordCounterMain(
				"here is the root of the root and the bud of the bud");
		counter.count();
		System.out.println(counter.hasWord("root"));
		System.out.println(counter.hasWord("leaf"));

	}

	public void count() {
		String[] splitStr = message.split(" ");
		for (String word : splitStr) {

			if (wordCount.containsKey(word)) {
				wordCount.put(word, wordCount.get(word) + 1);
			} else {
				wordCount.put(word, 1);
			}
		}

	}

	public int hasWord(String word2) {
		if (wordCount.containsKey(word2) == true) {
			int sum = wordCount.get(word2);
			return sum;
		} else {
			return 0;
		}
	}
}
